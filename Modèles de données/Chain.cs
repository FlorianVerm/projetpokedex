﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Modèles_de_données
{
    public class Chain
    {
        [JsonPropertyName("evolves_to")]
        public List<EvolvesTo> Evolves_To { get; set; }

        [JsonPropertyName("species")]
        public Species3 Species { get; set; }
    }
}
