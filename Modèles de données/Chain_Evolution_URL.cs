﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Modèles_de_données
{
    public class Chain_Evolution_URL
    {

        [JsonPropertyName("evolution_chain")]
        public Evolution Evolution_chain { get; set; }
    }
}
