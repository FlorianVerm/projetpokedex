﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Modèles_de_données
{
    public class EvolvesTo
    {
   
        [JsonPropertyName("evolves_to")]
        public List<EvolvesTo2> evolves_To { get; set; }

        [JsonPropertyName("species")]
        public Species2 Species { get; set; }
    }
}
