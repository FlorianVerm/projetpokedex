﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Modèles_de_données
{
    public class EvolvesTo2
    {

        [JsonPropertyName("species")]
        public Species Species { get; set; }
    }

}
