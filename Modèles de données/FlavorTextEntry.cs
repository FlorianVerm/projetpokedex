﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Modèles_de_données
{
    public class FlavorTextEntry
    {
        [JsonPropertyName("flavor_text")]
        public string Flavor_Text { get; set; }

        [JsonPropertyName("language")]
        public Language Language { get; set; }
    }
}
