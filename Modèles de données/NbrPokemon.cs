﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Modèles_de_données
{
    public class NbrPokemon
    {
        [JsonPropertyName("count")]
        public int Count { get; set; }
    }
}
