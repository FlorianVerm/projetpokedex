﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;


namespace Modèles_de_données
{
    public class Pokemon
    {

        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("types")]
        public List<Type> Types { get; set; }

        [JsonPropertyName("flavor_text_entries")]
        public List<FlavorTextEntry> Flavor_text_entries { get; set; }

        [JsonPropertyName("chain")]
        public Chain Chain { get; set; }


    }
}
