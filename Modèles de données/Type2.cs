﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Modèles_de_données
{
    public class Type2
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
    }
}
