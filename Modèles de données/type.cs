﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Modèles_de_données
{
    public class Type
    {
        [JsonPropertyName("type")]
        public Type2 type { get; set; }
    }
}
