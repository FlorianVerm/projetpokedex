﻿using Modèles_de_données;
using Newtonsoft.Json;
using Récupération_des_données;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProjetPokedex
{
   
    public class Program
    {
        static async Task Main(string[] args)
        {

            RecupDonnées recup = new RecupDonnées();

            Task<string> maTacheRetournantUnString3 = recup.RecupNbreTotalDePokemon();

            string Res4 = await maTacheRetournantUnString3;

            NbrPokemon DeserializedObject2 = JsonConvert.DeserializeObject<NbrPokemon>(Res4);
            

            Console.WriteLine("\n========== Pokemon API ==========");
            Console.WriteLine("Jusqu a quel pokemon souhaitez vous chercher ? (ID max 201) ");
            int IDMaxInt = 0;
            do
            {
                if (IDMaxInt > 201)
                {
                    Console.WriteLine("IdMax trop Grand");
                }

                if (IDMaxInt < 0)
                {
                    Console.WriteLine("IdMax trop petit");
                }

                string IDMax = Console.ReadLine();
                int.TryParse(IDMax, out IDMaxInt);
            }
            while (IDMaxInt > 201 || IDMaxInt < 0);
            Console.WriteLine("Chargement en cours... Veuillez patienter");

            Pokemon[] pokemon = new Pokemon[IDMaxInt+1];

            //permet de recuperer les pokemons dans un tableau
            for (int i = 1; i <= IDMaxInt; i++)
            {
                Task<string> maTacheRetournantUnString = recup.RecupPokemon(i);
                Task<string> maTacheRetournantUnString1 = recup.RecupDescriptionEtUrlChaineEvolutionPokemon(i);
                string Res = await maTacheRetournantUnString;
                string Res1 = await maTacheRetournantUnString1;

                Chain_Evolution_URL DeserializedObject = JsonConvert.DeserializeObject<Chain_Evolution_URL>(Res1);

                Task<string> maTacheRetournantUnString2 = recup.RecupChaineEvolutionPokemon(DeserializedObject.Evolution_chain.url);
                string Res2 = await maTacheRetournantUnString2;

                Res1 = Res1.Remove(0, 1);
                Res1 = Res1.Remove(Res1.Length - 1, 1);

                Res2 = Res2.Remove(0, 1);
                Res2 = Res2.Remove(Res2.Length - 8, 7);


                string Res3 = Res.Remove(Res.Length - 1, 1) + "," + "\n" + Res1 + "," + "\n" + Res2;


                Pokemon DeserializedObject1 = JsonConvert.DeserializeObject<Pokemon>(Res3);

                pokemon[i] = DeserializedObject1;

            }

            while (true)
            {
                Console.WriteLine("\n========== Pokemon API ==========");

                Console.WriteLine("1 - Chercher un pokemon par son nom");
                Console.WriteLine("2 - Lister les pokemons");
                Console.WriteLine("0 - Quitter le programme");
                Console.Write("Choix : ");
                string Choix = Console.ReadLine();
                Console.WriteLine("=================================");

               
                int.TryParse(Choix, out int Result);
                if (Result == 1)
                {
                    Console.WriteLine("Nom (en anglais) : ");
                    string NomPokemon = Console.ReadLine().ToLower();

                    string PokemonRechercher = "";
                    //Permets de Rechercher un Pokemon avec son nom(écrire le pokemon en minuscule et en anglais) est d'obtenir son id dans le tableau qui est également son id dans le pokedex pour pouvoir obtenir ces infos
                    for (int i = 1; i <= IDMaxInt; i++)
                    {
                        if (pokemon[i].Name.Equals(NomPokemon))
                        {
                            PokemonRechercher = pokemon[i].Id;
                        }
                    }
                    if(PokemonRechercher != "")
                    {
                        int.TryParse(PokemonRechercher, out int Recherche);

                        Console.WriteLine("\n>>> Informations Pokemon <<<\n");

                        //afficher l'id du pokemon
                        Console.WriteLine("ID : " + pokemon[Recherche].Id);


                        //afficher le nom du pokemon
                        Console.WriteLine("\nName : " + pokemon[Recherche].Name);


                        //afficher les types du pokemon
                        Console.Write("\nTypes : ");
                        foreach (var val in pokemon[Recherche].Types)
                        {
                            Console.Write(val.type.Name);
                            Console.Write(", ");
                        }


                        //afficher la chaine d'évolution du pokemon
                        Console.WriteLine("\n\nChaine d'évolution : ");


                        if (pokemon[Recherche].Chain.Evolves_To.Count == 0)
                        {
                            Console.WriteLine(pokemon[Recherche].Chain.Species.Name);
                        }
                        else
                        {
                            if (pokemon[Recherche].Chain.Evolves_To[0].evolves_To.Count == 0)
                            {
                                Console.WriteLine(pokemon[Recherche].Chain.Species.Name);
                                Console.WriteLine(pokemon[Recherche].Chain.Evolves_To[0].Species.Name);
                            }
                            else
                            {
                                Console.WriteLine(pokemon[Recherche].Chain.Species.Name);
                                Console.WriteLine(pokemon[Recherche].Chain.Evolves_To[0].Species.Name);
                                Console.WriteLine(pokemon[Recherche].Chain.Evolves_To[0].evolves_To[0].Species.Name);

                            }

                        }


                        //afficher la description du pokemon
                        int compteur = 0;
                        Console.WriteLine("\nDescription : ");
                        foreach (var val1 in pokemon[Recherche].Flavor_text_entries)
                        {
                            if (val1.Language.Name == "fr" && compteur == 0)
                            {
                                Console.WriteLine(val1.Flavor_Text);
                                compteur = 1;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Pokemon non trouvé");
                    }


                }
                if (Result == 2)
                {
                    for (int i = 1; i <= IDMaxInt; i++)
                    {

                        Console.WriteLine(pokemon[i].Id + " " + pokemon[i].Name);
                    }
                }
                if(Result == 0 )
                {
                    Console.WriteLine("Vous quittez le programme");
                    System.Environment.Exit(1);
                }
              
            }
            
        }
     
    }
}
