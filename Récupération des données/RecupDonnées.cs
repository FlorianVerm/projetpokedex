﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Récupération_des_données
{
    public class RecupDonnées
    {

        public async Task<string> RecupPokemon(int idPoke)
        {

            using (var v_HttpClient = new HttpClient())
            {
                HttpResponseMessage response;
                try
                {
                    response = await
                    v_HttpClient.GetAsync("https://pokeapi.co/api/v2/pokemon/" + idPoke);
                    response.EnsureSuccessStatusCode();
                    string contentResponse = await response.Content.ReadAsStringAsync();
                    return contentResponse;
                }
                catch (HttpRequestException)
                {
                    throw;
                }
            }
        }

        public async Task<string> RecupDescriptionEtUrlChaineEvolutionPokemon(int idPoke)
        {

            using (var v_HttpClient = new HttpClient())
            {
                HttpResponseMessage response;
                try
                {
                    response = await
                    v_HttpClient.GetAsync("https://pokeapi.co/api/v2/pokemon-species/" + idPoke);
                    response.EnsureSuccessStatusCode();
                    string contentResponse = await response.Content.ReadAsStringAsync();
                    return contentResponse;
                }
                catch (HttpRequestException)
                {
                    throw;
                }
            }
        }
        public async Task<string> RecupChaineEvolutionPokemon(string url)
        {

            using (var v_HttpClient = new HttpClient())
            {
                HttpResponseMessage response;
                try
                {
                    response = await
                    v_HttpClient.GetAsync(url);
                    response.EnsureSuccessStatusCode();
                    string contentResponse = await response.Content.ReadAsStringAsync();
                    return contentResponse;
                }
                catch (HttpRequestException)
                {
                    throw;
                }
            }

        }

        public async Task<string> RecupNbreTotalDePokemon()
        {

            using (var v_HttpClient = new HttpClient())
            {
                HttpResponseMessage response;
                try
                {
                    response = await
                    v_HttpClient.GetAsync("https://pokeapi.co/api/v2/pokemon/");
                    response.EnsureSuccessStatusCode();
                    string contentResponse = await response.Content.ReadAsStringAsync();
                    return contentResponse;
                }
                catch (HttpRequestException)
                {
                    throw;
                }
            }
        }
    }
}